const readline = require('readline')

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})
const lines = []
rl.on('line', (input) => {
    lines.push(input);
})
rl.on('close', () => {
    let l = 0;
        const n = lines[l++].trim().split(' ').map(Number)
        for (var i = 0; i < n; i++) {
          const [a, b, c] = lines[l++].trim().split(' ').map(Number)
          console.log(solve(a, b, c))
        }
})

function solve(a, b, c) {
  var A = [a, b, c]
  A.sort((p, q) => p - q)
  const min = A[0]
  const mid = A[1]
  const max = A[2]
  if (min == max) {
    ans = 3
  } else if (min == mid || mid == max) {
    if (max - min == 1) ans = 2
    else ans = 1
  } else {
    if (max == b) ans = 0
    else if (max - mid == mid - min) {
      ans = b - min + 1
      if (ans >= b) ans = -1
    } else {
      ans = max - min + 1
      if (ans >= min) ans = -1
    }
  }
  return ans
}
