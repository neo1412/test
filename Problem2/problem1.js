const readline = require('readline')

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})
const lines = []
rl.on('line', (input) => {
    lines.push(input);
})
rl.on('close', () => {
    let l = 0;
        const [n, N] = lines[l++].trim().split(' ').map(Number)
        const A = lines[l++].trim().split(' ').map(Number)
        console.log(solve(n, N, A))
})

function solve(n, N, A) {
  A.sort((p, q) => p - q)
  if (A[0] <= N && N <= A[n-1]) {
      return "Yes"
  } else {
      return "No"
  }
}
