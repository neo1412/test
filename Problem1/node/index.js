const http = require('http');
const url = require('url');

const server = http.createServer((req, res) => {
	const queryObject = url.parse(req.url, true).query;
	console.log(queryObject.obj);

  // ここ以下のみ変える
	var object = JSON.parse(queryObject.obj);
  // var object = [
  //   { num: 4, text: 'fizz' },
  //   { num: 7, text: 'buzz' },
  //   { num: 8, text: 'hoge' },
  //   { num: 15, text: 'huga' }
  // ];
	const object_length = object.length;

  const start = 1;
  const end = 30;
	var answer = [];
	for (var i = start; i <= end; i++) {
		answer.push(i);
	}

	for (var i = 0; i < object_length; i++) {
		for (var number = start; number <= end - start + 1; number++) {
			if (number % object[i].num == 0) {
				if (Number.isInteger(answer[number - 1])) {
					answer[number - 1] = object[i].text;
				} else {
					answer[number - 1] += ' ' + object[i].text;
				}
			}
		}
	}

	var ans = '';
	for (var i = 0; i < 30; i++) {
		if (i == 0) ans += answer[i];
		else ans += ', ' + answer[i];
	}
  
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
	res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST');
	res.setHeader('Access-Control-Allow-Headers', '*');
	res.writeHead(200, {
		'Content-Type': 'text/html'
	});
	res.write(ans);
	res.end();
});
server.listen(8080);
